package br.com.wiretecnologia.spartacus.core.repositories;

import br.com.wiretecnologia.spartacus.core.entities.ParceiroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParceiroRepositoy extends JpaRepository<ParceiroEntity, Long> {
}
