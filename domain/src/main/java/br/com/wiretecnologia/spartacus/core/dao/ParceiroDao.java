package br.com.wiretecnologia.spartacus.core.dao;

import br.com.wiretecnologia.spartacus.core.dao.impl.DaoAbstract;
import br.com.wiretecnologia.spartacus.core.entities.ParceiroEntity;
import br.com.wiretecnologia.spartacus.core.repositories.ParceiroRepositoy;
import org.springframework.stereotype.Component;

@Component
public class ParceiroDao extends DaoAbstract<ParceiroEntity, Long> {

    private final ParceiroRepositoy parceiroRepositoy;

    public ParceiroDao(ParceiroRepositoy parceiroRepositoy) {
        super(parceiroRepositoy);
        this.parceiroRepositoy = parceiroRepositoy;
    }
}
