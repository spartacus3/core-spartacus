package br.com.wiretecnologia.spartacus.core.repositories;

import br.com.wiretecnologia.spartacus.core.entities.ProdutoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {

}
