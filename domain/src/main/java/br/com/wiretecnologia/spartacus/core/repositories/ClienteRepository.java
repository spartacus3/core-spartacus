package br.com.wiretecnologia.spartacus.core.repositories;


import br.com.wiretecnologia.spartacus.core.entities.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Long> {
}
