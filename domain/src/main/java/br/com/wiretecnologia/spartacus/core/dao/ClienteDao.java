package br.com.wiretecnologia.spartacus.core.dao;

import br.com.wiretecnologia.spartacus.core.dao.impl.DaoAbstract;
import br.com.wiretecnologia.spartacus.core.entities.ClienteEntity;
import br.com.wiretecnologia.spartacus.core.repositories.ClienteRepository;
import org.springframework.stereotype.Component;

@Component
public class ClienteDao extends DaoAbstract<ClienteEntity, Long> {

    private final ClienteRepository clienteRepository;

    public ClienteDao(ClienteRepository clienteRepository) {
        super(clienteRepository);
        this.clienteRepository = clienteRepository;
    }

}
