package br.com.wiretecnologia.spartacus.core.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "PARCEIRO")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParceiroEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CODIGO")
    private Long codigo;

    @Column(name = "CNPJ")
    private Long cnpj;

    @Column(name = "RAZAO_SOCIAL")
    private String razaoSocial;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CEP")
    private Long cep;

    @Column(name = "LOGRADOURO")
    private String logradouro;

    @Column(name = "BAIRRO")
    private String bairro;

    @Column(name = "NUMERO")
    private Long numero;

    @Column(name = "COMPLEMENTO")
    private String complemento;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "UF")
    private String uf;

    @Column(name = "NOME_REPRESENTANTE")
    private String nomeRepresentante;

    @Column(name = "CONTATO")
    private Long contato;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "COD_PRODUTO")
    private Long cod_produto;
}
