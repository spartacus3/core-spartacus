package br.com.wiretecnologia.spartacus.core.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "PRODUTO")
@Getter
@Setter
@NoArgsConstructor
public class ProdutoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CODIGO")
    private Long codigo;

    @Column(name = "NOME_PRODUTO")
    private String nomeProduto;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "VALOR_UNITARIO")
    private float valorUnitario;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "IMAGEM")
    private byte[] imagem;

    @Column(name = "COD_PARCEIRO")
    private Long cod_parceiro;

}
