package br.com.wiretecnologia.spartacus.core.dao.impl.interfaces;

import java.util.List;
import java.util.Optional;

public interface DaoInterface<T, D> {

    T saveAndFlush(T entity);

    T save(T entity);

    List<T> saveAll(List<T> entities);

    List<T> findAll();

    void delete(T entity);

    void deleteAll(List<T> entities);

    void flush();

    Optional<T> findById(D id);
}
