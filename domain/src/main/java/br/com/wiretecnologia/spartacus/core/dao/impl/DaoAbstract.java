package br.com.wiretecnologia.spartacus.core.dao.impl;


import br.com.wiretecnologia.spartacus.core.dao.impl.interfaces.DaoInterface;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class DaoAbstract<T, D> implements DaoInterface<T, D> {

    private final JpaRepository<T, D> jpaRepository;

    public DaoAbstract(JpaRepository jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    @Override
    public T saveAndFlush(T entity) {
        return jpaRepository.saveAndFlush(entity);
    }

    @Override
    public T save(T entity) {
        return jpaRepository.save(entity);
    }

    @Override
    public List<T> saveAll(List<T> entities) {
        return jpaRepository.saveAll(entities);
    }

    @Override
    public List<T> findAll() {
        return jpaRepository.findAll();
    }

    @Override
    public void delete(T entity) {
        jpaRepository.delete(entity);
    }

    @Override
    public void deleteAll(List<T> entities) {
        jpaRepository.deleteAll(entities);
    }

    @Override
    public void flush() {
        jpaRepository.flush();
    }

    @Override
    public Optional<T> findById(D id) {
        return jpaRepository.findById(id);
    }
}
