package br.com.wiretecnologia.spartacus.core.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayloadDefaultAbstract<T> {

    @JsonProperty
    private List<T> elements;

    @JsonProperty
    private T element;

    @JsonProperty
    private Integer totalPages = 0;

    @JsonProperty
    private Integer qtdInPage = 0;

    @JsonProperty
    private Integer page = 0;

    @JsonProperty
    private Long totalElements = 0L;

    @JsonProperty
    private String error = "";

    public PayloadDefaultAbstract() {

    }

    public PayloadDefaultAbstract(T element) {
        this.element = element;
        setPage(null);
        setTotalPages(null);
        setQtdInPage(null);
        setTotalElements(null);
    }

    public PayloadDefaultAbstract(List<T> elements) {
        this.elements = elements;

        if (!elements.isEmpty()) {
            setTotalElements(Long.valueOf(elements.size()));
        }
    }

    public PayloadDefaultAbstract(Page<T> elements) {
        List<T> elementsList = elements.stream().collect(Collectors.toList());

        setElements(elementsList);
        setTotalPages(elements.getTotalPages());
        setTotalElements(elements.getTotalElements());
        setQtdInPage(elements.getNumberOfElements());
        setPage(elements.getNumber() + 1);
    }

    public PayloadDefaultAbstract(String error) {
        setError(error);
        setPage(null);
        setTotalPages(null);
        setQtdInPage(null);
        setTotalElements(null);
    }

    public List<T> getElements() {
        return elements;
    }

    public void setElements(List<T> elements) {
        this.elements = elements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getQtdInPage() {
        return qtdInPage;
    }

    public void setQtdInPage(Integer qtdInPage) {
        this.qtdInPage = qtdInPage;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
