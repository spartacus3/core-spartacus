package br.com.wiretecnologia.spartacus.core.entities;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "CLIENTE")
@Getter
@Setter
@NoArgsConstructor
public class ClienteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CODIGO")
    private Long codigo;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "RAZAO_SOCIAL")
    private String razaoSocial;

    @Column(name = "CPF")
    private int cpf;

    @Column(name = "CNPJ")
    private int cnpj;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "CEP")
    private int cep;

    @Column(name = "RUA")
    private String rua;

    @Column(name = "NUMERP")
    private int numero;

    @Column(name = "BAIRRO")
    private String bairro;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "UF")
    private String uf;

    @Column(name = "TELEFONE")
    private int telefone;

    @Column(name = "CELULAR")
    private int celular;

    @Column(name = "EMAIL")
    private String email;
}
