package br.com.wiretecnologia.spartacus.core.dao;

import br.com.wiretecnologia.spartacus.core.dao.impl.DaoAbstract;
import br.com.wiretecnologia.spartacus.core.entities.ProdutoEntity;
import br.com.wiretecnologia.spartacus.core.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProdutoDao extends DaoAbstract<ProdutoEntity, Long> {

    private final ProdutoRepository produtoRepository;

    @Autowired
    public ProdutoDao(ProdutoRepository produtoRepository) {
        super(produtoRepository);
        this.produtoRepository = produtoRepository;
    }
}
