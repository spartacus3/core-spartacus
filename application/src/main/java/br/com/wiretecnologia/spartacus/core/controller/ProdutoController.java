package br.com.wiretecnologia.spartacus.core.controller;

import br.com.wiretecnologia.spartacus.core.dao.ProdutoDao;
import br.com.wiretecnologia.spartacus.core.entities.ProdutoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v1/produto")
@Slf4j
public class ProdutoController {

    private final ProdutoDao produtoDao;

    @Autowired
    public ProdutoController(ProdutoDao produtoDao) {
        this.produtoDao = produtoDao;
    }


    @GetMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity findById(@PathVariable("id") Long id) {

        log.info("BUSCASCANDO POR NOME\n");
        Optional<ProdutoEntity> produto = produtoDao.findById(id);


        if (produto.isEmpty()) {
            return ResponseEntity.status(404).body("Produto não encontrado");
        } else {
            return ResponseEntity.ok(produto);
        }
    }


    @PostMapping()
    public @ResponseBody
    ResponseEntity insere(@Valid @RequestBody ProdutoEntity produto) {

        log.info("INSERINDO SUCESSO\n");
        try {
            ProdutoEntity produtoEntity = produtoDao.saveAndFlush(produto);
            return ResponseEntity.status(200).body(produtoEntity);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Erro ao cadastrar novo produto");
        }
    }

    @PutMapping()
    public @ResponseStatus
    ResponseEntity alterarProduto(@Valid @RequestBody ProdutoEntity produto) {
        Optional<ProdutoEntity> produtoEntity = produtoDao.findById(produto.getCodigo());
        log.info("# Alterando produto");
        if (produtoEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Produto-Id:".concat(produto.getCodigo().toString()).concat(" não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Produto-Id:".concat(produto.getCodigo().toString()).concat(" Alterado com sucesso"));

        }
    }

    @DeleteMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity deletarProduto(@PathVariable("id") Long id) {
        log.info("# Deletetando E \n");
        Optional<ProdutoEntity> produtoEntity = produtoDao.findById(id);

        if (produtoEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Produto-Id:".concat(id.toString()).concat("não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Produto-Id:".concat(id.toString()).concat("deletado com sucesso"));

        }
    }

}
