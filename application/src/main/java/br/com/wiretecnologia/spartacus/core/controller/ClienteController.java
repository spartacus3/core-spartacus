package br.com.wiretecnologia.spartacus.core.controller;


import br.com.wiretecnologia.spartacus.core.dao.ClienteDao;
import br.com.wiretecnologia.spartacus.core.entities.ClienteEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v1/cliente")
@Slf4j
public class ClienteController {

    private final ClienteDao clienteDao;

    @Autowired
    public ClienteController(ClienteDao clienteDao) {
        this.clienteDao = clienteDao;
    }


    @GetMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity findById(@PathVariable("id") Long id) {

        log.info("BUSCASCANDO POR NOME\n");
        Optional<ClienteEntity> produto = clienteDao.findById(id);


        if (produto.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(produto);
        }
    }


    @PostMapping()
    public @ResponseBody
    ResponseEntity insere(@Valid @RequestBody ClienteEntity produto) {

        log.info("INSERINDO SUCESSO\n");
        try {
            ClienteEntity produtoEntity = clienteDao.saveAndFlush(produto);
            return ResponseEntity.status(200).body(produtoEntity);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Erro ao cadastrar novo produto");
        }


    }

    @PutMapping()
    public @ResponseStatus
    ResponseEntity getAll(@Valid @RequestBody ClienteEntity produto) {
        Optional<ClienteEntity> clienteEntity = clienteDao.findById(produto.getCodigo());

        if (clienteEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Produto-Id:".concat(produto.getCodigo().toString()).concat("não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Produto-Id:".concat(produto.getCodigo().toString()).concat("Alterado com sucesso"));

        }
    }

    @DeleteMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity getEstoque(@PathVariable("id") Long id) {
        log.info("BUSCA QUANTIDADE MININA DE ESTOQUE \n");
        Optional<ClienteEntity> clienteEntity = clienteDao.findById(id);

        if (clienteEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Produto-Id:".concat(id.toString()).concat("não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Produto-Id:".concat(id.toString()).concat("deletado com sucesso"));

        }
    }
}
