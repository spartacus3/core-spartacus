package br.com.wiretecnologia.spartacus.core.controller;


import br.com.wiretecnologia.spartacus.core.dao.ParceiroDao;
import br.com.wiretecnologia.spartacus.core.entities.ParceiroEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v1/parceiro")
@Slf4j
public class ParceiroController {

    private final ParceiroDao parceiroDao;

    @Autowired
    public ParceiroController(ParceiroDao parceiroDao) {
        this.parceiroDao = parceiroDao;
    }

    @GetMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity findById(@PathVariable("id") Long id) {

        log.info("# Buscando por id:\n");
        Optional<ParceiroEntity> produto = parceiroDao.findById(id);


        if (produto.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(produto);
        }
    }


    @PostMapping()
    public @ResponseBody
    ResponseEntity insere(@Valid @RequestBody ParceiroEntity parceiroEntity) {

        try {
            log.info("# INSERINDO SUCESSO\n");
            ParceiroEntity produtoEntity = parceiroDao.saveAndFlush(parceiroEntity);
            return ResponseEntity.status(200).body(produtoEntity);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Erro ao cadastrar novo produto");
        }


    }

    @PutMapping()
    public @ResponseStatus
    ResponseEntity alterarProduto(@Valid @RequestBody ParceiroEntity parceiroEntity) {
        Optional<ParceiroEntity> produtoEntity = parceiroDao.findById(parceiroEntity.getCodigo());

        if (produtoEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Produto-id:".concat(parceiroEntity.getCodigo().toString()).concat("não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Produto-Id:".concat(parceiroEntity.getCodigo().toString()).concat("Alterado com sucesso"));

        }
    }

    @DeleteMapping(value = "/{id}")
    public @ResponseStatus
    ResponseEntity getEstoque(@PathVariable("id") Long id) {
        log.info("# Deletando \n");
        Optional<ParceiroEntity> produtoEntity = parceiroDao.findById(id);

        if (produtoEntity.isEmpty()) {
            return ResponseEntity.status(404).body("Parceiro-Id:".concat(id.toString()).concat("não encontrado"));
        } else {
            return ResponseEntity.status(200).body("Parceiro-Id:".concat(id.toString()).concat("deletado com sucesso"));

        }
    }
}
